package com.tarun.test.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tarun.test.beans.EmployeeBean;
import com.tarun.test.services.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping(value = "/create", produces = MediaType.ALL_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createEmployee(@RequestBody EmployeeBean employeeBean) throws Exception {
		return new ResponseEntity<Object>(employeeService.createEmployee(employeeBean), HttpStatus.OK);
	}

	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateEmployee(@RequestBody EmployeeBean employeeBean,
			@RequestHeader(name = "authKey") String authKey) throws Exception {
		return new ResponseEntity<Object>(employeeService.updateEmployee(employeeBean, authKey), HttpStatus.OK);
	}

	@GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> employeeLogin(@RequestHeader String email, @RequestHeader String password)
			throws Exception {
		return new ResponseEntity<Object>(employeeService.employeeLogin(email, password), HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{employeeId}", produces = MediaType.ALL_VALUE)
	public ResponseEntity<?> deleteEmployee(@PathVariable(name = "employeeId") String employeeId,
			@RequestHeader String authKey) throws Exception {
		return new ResponseEntity<Object>(employeeService.deleteEmployee(employeeId, authKey), HttpStatus.OK);
	}
}
