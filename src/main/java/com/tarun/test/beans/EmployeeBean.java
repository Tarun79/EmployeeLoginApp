package com.tarun.test.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeBean {
	private String id;
	private String address;
	private String email;
	private String mobileNo;
	private String name;
	private String password;

	public EmployeeBean(String id, String address, String email, String mobileNo, String name, String password) {
		this.id = id;
		this.address = address;
		this.email = email;
		this.mobileNo = mobileNo;
		this.name = name;
		this.password = password;

	}

	public EmployeeBean() {
		super();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;

	}

	@Override
	public String toString() {
		return "EmployeeBean [id=" + id + ", address=" + address + ", email=" + email + ", mobileNo=" + mobileNo
				+ ", name=" + name + "]";
	}
}
