package com.tarun.test;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.tarun.test.controllers.EmployeeControllerTest;

@SpringBootTest(classes = EmployeeControllerTest.class)
class DemoApplicationTests {

	@Test
	void contextLoads() {
		System.out.println("hello this is test...");
	}

}
