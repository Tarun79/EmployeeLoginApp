package com.tarun.test.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarun.test.beans.EmployeeBean;
import com.tarun.test.beans.LoginBean;
import com.tarun.test.models.Employee;
import com.tarun.test.models.LoginDetail;
import com.tarun.test.repositories.EmployeeRepository;
import com.tarun.test.repositories.LoginDetailRepository;
import com.tarun.test.utility.GeneralUtility;
import com.tarun.test.utility.UtilsConverter;
import com.tarun.test.utility.UtilsValidator;

@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	LoginDetailRepository loginDetailRepository;

	public String createEmployee(EmployeeBean employeeBean) throws Exception {
		UtilsValidator.employeeBeanValidator(employeeBean, false);
		Employee employee = new Employee(employeeBean.getName(), employeeBean.getAddress(), employeeBean.getMobileNo(),
				employeeBean.getEmail(), GeneralUtility.generateEncriptedPassword(employeeBean.getPassword()));
		employeeRepository.save(employee);
		return "Employee created successfully..";
	}

	public EmployeeBean updateEmployee(EmployeeBean employeeBean, String authKey) throws Exception {
		if (employeeBean.getId().isBlank()) {
			throw new Exception("Employee id must not be null or empty..");
		}
		UtilsValidator.employeeBeanValidator(employeeBean, true);
		Employee employee = employeeRepository.findByEmpId(employeeBean.getId());

		isAutheticatedEmployee(employee, authKey);

		employee.setAddress(employeeBean.getAddress());
		employee.setEmail(employeeBean.getEmail());
		employee.setMobileNo(employeeBean.getMobileNo());
		employee.setName(employeeBean.getName());

		employee = employeeRepository.save(employee);
		return UtilsConverter.convertEmployeeToEmployeeBean(employee);
	}

	public LoginBean employeeLogin(String email, String password) throws Exception {

		UtilsValidator.logInValidator(email, password, false);
		Employee employee = employeeRepository.findByEmailAndPassword(email,
				GeneralUtility.generateEncriptedPassword(password));

		isEmployeeExist(employee);

		List<LoginDetail> loginDetails = loginDetailRepository.findByEmployeeIdOrderByLoginAtDesc(employee);
		LoginDetail loginDetail = null;

		if (!loginDetails.isEmpty()) {
			loginDetail = loginDetails.get(0);
		}
		if (null == loginDetail || isLogInExpired(loginDetail.getLoginAt())) {
			loginDetail = new LoginDetail(employee);
			loginDetail = loginDetailRepository.save(loginDetail);
		}
		return UtilsConverter.convertLoginDetailToLoginBean(loginDetail);
	}

	public String deleteEmployee(String employeeId, String authKey) throws Exception {
		Employee employee = employeeRepository.findByEmpId(employeeId);

		isAutheticatedEmployee(employee, authKey);

		employee.setIsDeleted(Boolean.TRUE);
		employee = employeeRepository.save(employee);
		return "Employee deleted successfully...";
	}

	private boolean isLogInExpired(Date loginAt) {
		Date currentTime = new Date();
		long diff = currentTime.getTime() - loginAt.getTime();
		return diff > 60 * 60 * 1000;
	}

	private void isAutheticatedEmployee(Employee employee, String authKey) throws Exception {
		isEmployeeExist(employee);
		LoginDetail loginDetail = loginDetailRepository.findByEmpId(authKey);

		if (!employee.getId().equals(loginDetail.getEmployeeId().getId())) {
			throw new Exception("You are not login user");
		}

		if (null == loginDetail || isLogInExpired(loginDetail.getLoginAt())) {
			throw new Exception("Employee login session expired, please login again...");
		}
	}

	private void isEmployeeExist(Employee employee) throws Exception {
		if (null == employee || employee.getIsDeleted()) {
			throw new Exception("Employee does not exist...");
		}
	}

}
