package com.tarun.test.models;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "login_detail")
public class LoginDetail {
	@Id
	@Column(name = "id", nullable = false, length = 36)
	private String id;

	@ManyToOne
	@JoinColumn(name = "employee_id", nullable = false)
	private Employee employeeId;

	@Column(name = "login_at", nullable = false)
	private Date loginAt;

	public LoginDetail(Employee employeeId) {
		this.employeeId = employeeId;
	}

	public LoginDetail() {
		super();
	}

	@PrePersist
	public void prePersist() {
		loginAt = new Date();
		id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Employee getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}

	public Date getLoginAt() {
		return loginAt;
	}

	public void setLoginAt(Date loginAt) {
		this.loginAt = loginAt;
	}

	@Override
	public String toString() {
		return "LoginDetail [id=" + id + ", loginAt=" + loginAt + "]";
	}

}
