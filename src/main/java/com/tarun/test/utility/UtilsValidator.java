package com.tarun.test.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tarun.test.beans.EmployeeBean;

public class UtilsValidator {
	public static void employeeBeanValidator(EmployeeBean employeeBean, boolean isUpdate) throws Exception {

		logInValidator(employeeBean.getEmail(), employeeBean.getPassword(), isUpdate);
		if (employeeBean.getAddress().isBlank() || employeeBean.getMobileNo().isBlank()
				|| employeeBean.getName().isBlank()) {
			throw new Exception("empty fields not allow..");
		}
		if (!isMobileNumberValid(employeeBean.getMobileNo())) {
			throw new Exception("Invalid mobile number not allow..");
		}
		if (employeeBean.getName().length() < 5) {
			throw new Exception("Name must be greater than 4 charecter...");
		}
	}

	public static void logInValidator(String email, String password, boolean isUpdate) throws Exception {
		if (email.isBlank()) {
			throw new Exception("empty fields not allow..");
		}

		if (!isEmailValid(email)) {
			throw new Exception("Invalid email not allow..");
		}
		if (!isUpdate) {
			if (password.isBlank()) {
				throw new Exception("empty fields not allow..");
			}
			if (password.length() < 5) {
				throw new Exception("password must be greater than 4 charecter...");
			}
		}
	}

	public static boolean isMobileNumberValid(String mNo) {
		Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");

		Matcher m = p.matcher(mNo);
		return (m.find() && m.group().equals(mNo));
	}

	public static boolean isEmailValid(String email) {
		Pattern p = Pattern.compile(
				"^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$");
		Matcher m = p.matcher(email);
		return m.matches();
	}

}
