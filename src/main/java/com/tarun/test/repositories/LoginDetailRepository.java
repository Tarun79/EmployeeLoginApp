package com.tarun.test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tarun.test.models.Employee;
import com.tarun.test.models.LoginDetail;

public interface LoginDetailRepository extends JpaRepository<LoginDetail, String> {
	@Query("select ld from LoginDetail ld where ld.id=:id")
	LoginDetail findByEmpId(@Param("id") String id);
	
	List<LoginDetail> findByEmployeeIdOrderByLoginAtDesc(Employee emp);
}
