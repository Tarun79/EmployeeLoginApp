package com.tarun.test.models;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId", fetch = FetchType.LAZY)
	private Collection<LoginDetail> LoginDetails;

	@Id
	@Column(name = "id", nullable = false, length = 36)
	private String id;

	@Column(name = "name", nullable = false, length = 200)
	private String name;

	@Column(name = "address", nullable = false, length = 400)
	private String address;

	@Column(name = "mobile_no", nullable = false, length = 15, unique = true)
	private String mobileNo;

	@Column(name = "email", nullable = false, length = 100, unique = true)
	private String email;

	@Column(name = "password", nullable = false, length = 150)
	private String password;

	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "updated_at", nullable = false)
	private Date updatedAt;

	@Column(name = "is_deleted", nullable = false)
	private Boolean isDeleted = Boolean.FALSE;

	public Employee(String name, String address, String mobileNo, String email, String password) {
		super();
		this.name = name;
		this.address = address;
		this.mobileNo = mobileNo;
		this.email = email;
		this.password = password;

	}

	public Employee() {
		super();
	}

	@PrePersist
	public void prePersist() {
		createdAt = updatedAt = new Date();
		id = UUID.randomUUID().toString();
	}

	@PreUpdate
	public void preUpdate() {
		updatedAt = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Collection<LoginDetail> getLoginDetails() {
		return LoginDetails;
	}

	public void setLoginDetails(Collection<LoginDetail> loginDetails) {
		LoginDetails = loginDetails;
	}

	@Override
	public String toString() {
		return "Employee [LoginDetails=" + LoginDetails + ", id=" + id + ", name=" + name + ", address=" + address
				+ ", mobileNo=" + mobileNo + ", email=" + email + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + ", isDeleted=" + isDeleted + "]";
	}
}
