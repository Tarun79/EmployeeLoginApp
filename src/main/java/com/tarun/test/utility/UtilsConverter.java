package com.tarun.test.utility;

import com.tarun.test.beans.EmployeeBean;
import com.tarun.test.beans.LoginBean;
import com.tarun.test.models.Employee;
import com.tarun.test.models.LoginDetail;

public class UtilsConverter {

	public static EmployeeBean convertEmployeeToEmployeeBean(Employee employee) {
		EmployeeBean employeeBean=new EmployeeBean();

		employeeBean.setId(employee.getId());
		employeeBean.setAddress(employee.getAddress());
		employeeBean.setEmail(employee.getEmail());
		employeeBean.setMobileNo(employee.getMobileNo());
		employeeBean.setName(employee.getName());

		return employeeBean;
	}

	public static LoginBean convertLoginDetailToLoginBean(LoginDetail loginDetail) {
		LoginBean loginBean=new LoginBean(loginDetail.getId(),loginDetail.getLoginAt(),convertEmployeeToEmployeeBean(loginDetail.getEmployeeId()));
		return loginBean;
	}

}
