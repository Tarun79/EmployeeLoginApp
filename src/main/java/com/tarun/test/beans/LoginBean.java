package com.tarun.test.beans;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginBean {
	private String authKey;
	private Date loginTime;
	private EmployeeBean employeeBean;

	public LoginBean(String authKey, Date loginTime, EmployeeBean employeeBean) {
		this.authKey = authKey;
		this.loginTime = loginTime;
		this.employeeBean = employeeBean;
	}
	public LoginBean() {

	}
	public String getAuthKey() {
		return authKey;
	}


	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}


	public Date getLoginTime() {
		return loginTime;
	}


	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}


	public EmployeeBean getEmployeeBean() {
		return employeeBean;
	}
	public void setEmployeeBean(EmployeeBean employeeBean) {
		this.employeeBean = employeeBean;
	}
	@Override
	public String toString() {
		return "LoginBean [authKey=" + authKey + ", loginTime=" + loginTime + ", employeeBean=" + employeeBean + "]";
	}

}
