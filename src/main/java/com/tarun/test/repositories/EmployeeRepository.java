package com.tarun.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tarun.test.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String> {
	Employee findByEmailAndPassword(String email,String password);
	@Query("select e from Employee e where e.id=:id")
	Employee findByEmpId(@Param("id") String id);
}
