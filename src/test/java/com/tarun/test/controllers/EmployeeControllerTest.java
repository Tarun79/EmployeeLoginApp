package com.tarun.test.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tarun.test.beans.EmployeeBean;
import com.tarun.test.repositories.EmployeeRepository;
import com.tarun.test.services.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private EmployeeController employeeController;

	@Mock
	EmployeeService employeeService;

	@Mock
	EmployeeRepository employeeRepository;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(employeeController).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testCreateEmployee() throws Exception {
		EmployeeBean employeeBean = new EmployeeBean();

		employeeBean.setAddress("indore");
		employeeBean.setEmail("abc102@gmail.com");
		employeeBean.setMobileNo("9519519514");
		employeeBean.setName("abc 451");
		employeeBean.setPassword("abc@451");
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(employeeBean);

		mockMvc.perform(MockMvcRequestBuilders.post("/employee/create").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson)).andExpect(status().isOk());
	}

}
